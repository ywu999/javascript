import MutualFunds from "./JPMorgan Mutual Funds.json";

export default function MutualFundParser() {
    const filtered = MutualFunds.filter((item) => { 
        return item.shareclassName.endsWith("-C");
    });
    console.log(filtered);
    return (
      <div>
      {filtered.map((el) => {
            return <div>{el.displayId} &nbsp; {el.shareclassName}</div>
      })}
      </div>
    );
}