import {Person} from './Person'
//default import 
// nameed import
//default export

export function promote() {}
export default class Teacher extends Person {
    constructor(name, degree) {
      super(name);
      this.degree = degree;
    }
    teach() {
        console.log(this.degree);
    }
  }