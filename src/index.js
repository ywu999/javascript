import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Teacher, {promote} from './Teacher';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// function hello() {
//  // for(var i=0; i<5; i++)
//   //console.log(i);
//   //var is accessible in the whole function. yaks!
//   //console.log(i);

//   //scope for the block
//   //for(let i=0; i<5; i++)
//   //console.log(i);
//   //Line 28:15:  'i' is not defined  no-undef
//   //console.log(i);

//   // const is also scoped for the block
//   //const x =1;
//   //Line 33:3:  'x' is assigned a value but never used  no-unused-vars 
//   //x=2;
//   // perfer const over let.

//   const person = {
//     name: 'Ryan',
//     walk: function() {
//       console.log(this);
//     },
//     talk(){}
//   };

//   person.walk();
//   const walk = person.walk.bind(person);
//   // the this will be bound to the person
//   walk();
//   // strict mode
//   // dot notation
//   // person.talk();
//   // person.name = 'John';

//   // const targetMember = 'name';
//   // person[targetMember] ='adam';
//   // console.log(person);
// }

// const square = function(number) {
//   return number*number;
// }

// const square1 = number => number*number;
// const square2 = (number) => {return number*number;}

// const data = [
//   {id: 1, isActive:true},
//   {id: 2, isActive:false},
//   {id:3, isActive:true}
// ]

// data.filter(function(input) {return input.isActive;});
// data.filter(data=>data.isActive);
// hello();

// A this inside a standalone function will be referrenced to the windows object.

// const person = {
//   walk() {
//     // var that = this;
//     var self = this;
//     setTimeout(function(){
//       console.log("this", self);
//     }, 1000);

//     //arrow function do not rebind this.
//     setTimeout(() => {console.log("this", this);
//     }, 1000);
//   }

// };

// person.walk();
// const colors = ["red", "blue", "yellow"];
//<li>Color</li>
//temlate
// const items = colors.map((color) => `<li>${color}</li>`);
// console.log(items);

// const address = {
//   street: '',
//   state: '',
//   country: ''
// };

// //const street = address.street;

// const {street, state, country} = address;
// alias of destruct
// const {street:st} = address;

//const first = [1,2,3];
//const second = [4,5,6];
//const combined1 = first.concat(second);
// spread operator
//const combined2 = [...first, 'a', ...start];
//const clone = [...first];

// const first = {name:'abc'};
// const second = {job:'somethign'};

// const c = {...first, ...second};
// console.log(c);

// class Person {
//   constructor(name) {
//     this.name = name;
//   }
//   walk() {

//   }
// }

// const person = new Person("Ryan");
// person.walk();

// class Teacher extends Person {
//   constructor(name, degree) {
//     super(name);
//     this.degree = degree;
//   }
//   teach() {

//   }
// }

const teacher = new Teacher("Jack", "Bachelor");
// Modular
teacher.teach();
promote();